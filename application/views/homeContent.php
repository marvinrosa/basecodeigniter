<?php

/*
	¿Qué es homeContent?
	Prácticamente un manejador de vistas,que permite trabajar de manera modular
	cada uno de las partes del html, Backend , Este esta divivo en:
	
	NOTA: el nombre de homeContent, no es representativo, simplemente el nombre que se le quiso dar.

	head 
	head_css
	nav
	header           | USO EXCLUSIVO con internalContent

	 - > contenido   | Elementos que se encuentran en la carpeta views>

	footer_js
	footer

	Importante:
	Ruta: application/views/
	
	backend/
		footer
		head
		header   //Elemento extra en la plantilla
		nav


	Notese, que dice: plantilla/backend
	Esto quiere decir, que en el controlador cuando necesitemos vistas de la plantilla del backend, en la linea siguiente:
						
						esta
	$this->load->view('homeContent',$data);

	Debe mandar a llamarse, para que cargue un conjunto de vistas anidadas que forman la plantilla del backend

*/


/*
 Esta linea permite que no se pueda acceder a este archivo por medio de la ruta
 USARLO SIEMPRE, para seguridad de los archivos
*/
defined('BASEPATH') OR exit('No direct script access allowed');

$this->load->view('plantilla/backend/head');
$this->load->view('plantilla/head_css');
$this->load->view('plantilla/backend/nav');
$this->load->view('plantilla/footer_js');
$this->load->view('vistas/'.$contenido);

// No es necesario el footer, queda a evaluación
//$this->load->view('plantilla/backend/footer');