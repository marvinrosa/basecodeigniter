<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<header id="header">
    <div class="container">
        <div class="row">
            <div class="col-md-10">
                <h1><span class="glyphicon glyphicon-cog" aria-hidden="true"></span> <?= $principal ?> <small> <?= $secundario ?></small></h1>
            </div>

        </div>
    </div>
</header>