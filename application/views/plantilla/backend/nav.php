
<?php
echo link_tag('css/style.css');
?>


<nav class="navbar navbar-default">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="home">PetCare</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">

            <ul class="nav navbar-nav">

                <?php if( isset($_SESSION['idRol']) && $_SESSION['idRol'] == 1 ): ?>
                    <li <?php if(isset($active) && $active == 'cliente'){ echo 'class="active"'; } ?>><a href="cliente">Inicio</a></li>

                    <li <?php if(isset($active) && $active == 'mis_mascotas'){ echo 'class="active"'; } ?>><a href="mis_mascotas">Mascotas</a></li>

                    <li <?php if(isset($active) && $active == 'cita_clientes'){ echo 'class="active"'; } ?>><a href="cita_clientes">Citas</a></li>


                <?php elseif( isset($_SESSION['idRol']) && $_SESSION['idRol'] == 2 ) : ?>


                    <li <?php if(isset($active) && $active == 'empleado'){ echo 'class="active"'; } ?>><a href="empleado">Inicio</a></li>
                    <li <?php if(isset($active) && $active == 'mascotas'){ echo 'class="active"'; } ?>><a href="mascotas">Mascotas</a></li>
                    <li <?php if(isset($active) && $active == 'cita_empleados'){ echo 'class="active"'; } ?>><a href="cita_empleados">Citas</a></li>
                    <li <?php if(isset($active) && $active == 'productos'){ echo 'class="active"'; } ?>><a href="productos">Productos</a></li>


                <?php elseif( isset($_SESSION['idRol']) && $_SESSION['idRol'] == 3 ) : ?>
                    <li <?php if(isset($active) && $active == 'admin'){ echo 'class="active"'; } ?>><a href="admin">Inicio</a></li>

                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown"> Mantenimiento <b class="caret"></b></a>

                        <ul class="dropdown-menu">

                            <li> <a href="mascotas_mantenimiento"> Mascotas  </a></li>
                            <li class="divider"></li>
                            <li> <a href="cita_root"> Citas  </a></li>
                            <li> <a href="servicios"> Servicios  </a></li>
                            <li class="divider"></li>
                            <li> <a href="categorias"> Categoria </a></li>
                            <li> <a href="proveedores"> Proveedor  </a></li>
                            <li> <a href="productos"> Productos </a></li>
                            <li class="divider"></li>
                            <li> <a href="usuarios"> Usuarios </a></li>
                            <li class="divider"></li>
                            <li><a href="#"> <span class="fa fa-external-link"></span> Configuración </a></li>

                        </ul>
                    </li>



                <?php else: ?>

                    <li <?php if(isset($active) && $active == 'inicio'){ echo 'class="active"'; } ?>><a href="./">Inicio</a></li>
                    <li <?php if(isset($active) && $active == 'acerca'){ echo 'class="active"'; } ?> ><a href="acerca">Acerca</a></li>
                    <li <?php if(isset($active) && $active == 'servicios'){ echo 'class="active"'; } ?>><a href="servicios">Servicios</a></li>
                    <li <?php if(isset($active) && $active == 'precios'){ echo 'class="active"'; } ?>><a href="precios">Precios</a></li>
                    <li <?php if(isset($active) && $active == 'contactenos'){ echo 'class="active"'; } ?>><a href="contactenos">Contáctenos</a></li>

                <?php endif; ?>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <?php if(!empty($_SESSION['logged_in'])): ?>
                   <li><a href="" data-toggle="modal" data-target="#info" data-toggle="tooltip" data-placement="left" title="Tooltip on left"><span class="fa fa-info"></span></a></li>
                    <li class="dropdown">

                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">  <?php  echo  $this->session->userdata('usuarioCorreo');?> <b class="caret"></b></a>

                        <ul class="dropdown-menu">
                            <li> <a href="cuenta"> <span class="glyphicon glyphicon-cog"></span> Cuenta</a></li>
                            <li> <a href="clave"> <span class="glyphicon glyphicon-lock"></span> Cambiar Contraseña </a></li>
                            <li class="divider"></li>

                            <li><a href="logout"> <span class="fa fa-external-link"></span> Salir</a></li>

                        </ul>
                    </li>

                <?php else: ?>

                    <li><a href="login">Iniciar Sesión </a></li>

                <?php endif; ?>
            </ul>


        </div><!--/.nav-collapse -->
    </div>
</nav>


<div id="info" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header login-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title">Add Project</h4>
            </div>
            <div class="modal-body">
                <input type="text" placeholder="Project Title" name="name">
                <input type="text" placeholder="Post of Post" name="mail">
                <input type="text" placeholder="Author" name="passsword">
                <textarea placeholder="Desicrption"></textarea>
            </div>
            <div class="modal-footer">
                <button type="button" class="cancel" data-dismiss="modal">Close</button>
                <button type="button" class="add-project" data-dismiss="modal">Save</button>
            </div>
        </div>

    </div>
</div>
