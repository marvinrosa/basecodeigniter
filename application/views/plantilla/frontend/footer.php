<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>


<footer>
    <div class="container" >
        <div class="row">
            <hr>
            <div class="col-lg-12">
                <div class="col-md-8">
                    <a href="#">Terms of Service</a> | <a href="#">Privacy</a>
                </div>
                <div class="col-md-4">
                    <p class="muted pull-right">© 2017 Empresa. All rights reserved</p>
                </div>
            </div>
        </div>
    </div>
</footer>


</body>
</html>