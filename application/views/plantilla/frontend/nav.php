<div class="wrap">
    <section class="top-section">
        <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
            <div class="container navbar-border">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse"> <span class="sr-only">Toggle navigation</span><span class="icon-bar"></span>
                        <span class="icon-bar"></span><span class="icon-bar"></span>
                    </button> <a class="navbar-brand" href="home">PQRS</a>

                </div>
                <div class="collapse navbar-collapse navbar-ex1-collapse">
                    <ul class="nav navbar-nav">
                        <li <?php if(isset($active) && $active == 'inicio'){ echo 'class="active"'; } ?>><a href="./">Inicio</a></li>
                        <li <?php if(isset($active) && $active == 'acerca'){ echo 'class="active"'; } ?> ><a href="">Acerca</a></li>
                       
                    </ul>
                    
                    <ul class="nav navbar-nav navbar-right">
                        <?php if(!empty($_SESSION['logged_in'])): ?>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">  <?php  echo  $this->session->userdata('usuarioCorreo');?> <b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <li> <a href="cuenta"> <span class="glyphicon glyphicon-cog"></span> Cuenta</a></li>

                                    <li class="divider"></li>

                                    <li><a href="logout"> <span class="fa fa-external-link"></span> Salir</a></li>

                                </ul>
                            </li>

                        <?php else: ?>

                            <li><a href="">Iniciar Sesión </a></li>

                        <?php endif; ?>
                    </ul>
                </div>
            </div>
        </nav>
    </section>
</div>