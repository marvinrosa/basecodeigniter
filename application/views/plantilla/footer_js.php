<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<script language="javaScript" type="text/javascript" src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>

<script language="javaScript"  type="text/javascript" src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>

<script language="javaScript"  type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

<script language="javaScript"  type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.js"></script>

<script language="javaScript" type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.11/js/jquery.dataTables.js"></script>

<script language="javaScript"  type="text/javascript" src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>

<script language="JavaScript" type="text/javascript" src="http://cdn.ckeditor.com/4.6.1/standard/ckeditor.js"></script>

<script language="JavaScript" type="text/javascript" src="js/jquery.mask.min.js"></script>

<script language="JavaScript" type="text/javascript" src="js/jquery.validate.min.js"></script>
<!-- Funciones Perzonalizadas -->
<script language="JavaScript" type="text/javascript" src="js/js-style.js"> </script>

<script type = 'text/javascript' src = "<?php echo base_url(); ?>js/conteo.js"></script>
<script type = 'text/javascript' src = "<?php echo base_url(); ?>js/usuarios.js"></script>