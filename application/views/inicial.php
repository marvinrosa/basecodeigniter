<?php

/*

	¿Qué es inicial?
	Prácticamente un manejador de vistas,que permite trabajar de manera modular
	cada uno de las partes del html,frontend, Este esta divivo en:
	
	NOTA: el nombre de inicial, no es representativo, simplemente el nombre que se le quiso dar.

	head 
	head_css
	nav

	 - > contenido   | Elementos que se encuentran en la carpeta views>

	footer_js
	footer

	Importante: En este importa el frontend, USO EXCLUSIVO FRONTEND
	Ruta: application/views/
	
	frontend/
		footer
		head
		nav


	Notese, que dice: plantilla/frontend
	Esto quiere decir, que en el controlador cuando necesitemos vistas de la plantilla del frontend, en la linea siguiente:
						
						esta
	$this->load->view('inicial',$data);

	Debe mandar a llamarse, para que cargue un conjunto de vistas anidadas que forman la plantilla del frontend


*/

/*
 Esta linea permite que no se pueda acceder a este archivo por medio de la ruta
 USARLO SIEMPRE, para seguridad de los archivos
*/
defined('BASEPATH') OR exit('No direct script access allowed');


$this->load->view('plantilla/frontend/head');
$this->load->view('plantilla/head_css');
$this->load->view('plantilla/frontend/nav');
$this->load->view('vistas/'.$contenido);
$this->load->view('plantilla/footer_js');

// No es necesario el footer, queda a evaluación
//$this->load->view('plantilla/frontend/footer');