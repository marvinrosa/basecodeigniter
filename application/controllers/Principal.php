
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Principal extends CI_Controller {

	/*
	 Controlador por defecto, en pocas palabras, aqui enpieza toda la magia: Principal

	 Importante a tomar en cuenta, a manera de buenas práctica escriber el nombre de los controladores con letra inicial mayuscula.

	 Cuando se manden a llamar con letra inicial minuscula

	 Archivos importantes que son importantes:

	 Ruta:application/config/
		
		autoload

		database        | Es importante a la hora de trabajar con la bd, al usar modelos

		routes    		| sumamente importante

	*/

	public function index()
	{
		$data = array();

		/*
			title

			Muestra el titulo de la página : Nombre en General de la aplicación
		*/
   		$data['title'] = "USAID";   
   		
		/*
			contenido

			Se mandan a llamar las vistas que se mostraran
			ver: application>views>vistas
			 * principal>index
		*/
   		$data['contenido']="principal/index"; 

   		/*
			active

			Propiedad de los li para sombrear la selección de un elemento.
			Para ver o editar los elementos del nav:
			 * Dirigirse:  application>views>plantilla>frontend>nav

			 Importante:
			   * Se trabaja de esta manera debido al tipo de plantilla que se tiene
			     del lado del frontend, cabe decir, que si se cambia a otro modelo distinto, la situación cambiaria.
		*/

        $data['active']="inicio";

        /*
        	inicial

        	Es un manejador de las vistas que estaran mostrando en el frontend, para hacer uso de los elementos que tiene la plntilla del frontend

        	Si se desea editar la plantilla del frontend, acceder a:
        	application>views>plantilla>frontend

        	Si se desea agregar una libreria de css o js, acceder a:
        	views > 
        		footer_js   -> Incluye todas las librerias de JS usados en todo el proyecto  (Jquery, js propios)
				
				head_css   -> Incluye todas las librerias de css de las que se haran uso (Bootstrap, css propios, etc)


        */
		$this->load->view('inicial',$data);
	}
}
